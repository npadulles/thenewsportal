require('dotenv').config();
const express = require('express');
const path = require('path');
const request = require('request');
const app = express();
const routes = require('./routes');

app.use(express.static(path.join(__dirname, 'build')));

//just for logging entry calls. should not be here for a real world app :)
app.use(function(req, res, next) {
  console.log("server request -> " + req.url);
  next();
});

//proxy to prevent CORS issue with client, and keep them simpler to match by workbox
app.get('/api/images', function(req,res) {
  var url = req.query.url;
  request(url).pipe(res);
});

//news api routes
app.use('/api', routes);

// Handles any requests that don't match the ones above
app.get('*', (req,res) =>{
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

//simple error handler
app.use(function(error, req, res, next) {
  if (error.status) {
    res.status(error.status);
  } else {
    res.status(400);
  }
  res.json({ message: error.message || error.toString() });
});

const port = process.env.PORT || 3001;
app.listen(port, () => console.log(`App running on port ${port} in ${process.env.NODE_ENV} mode!`));



