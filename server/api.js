const axios = require('axios');
const md5 = require('md5');

const apuUrl = process.env.API_URL || "https://newsapi.org/v2/top-headlines";
const apiKey = process.env.API_KEY || "no-key";
const country = process.env.API_COUNTRY || "us";

//used to generate a custom Id per article and add some extra params. also redirect image URLs to our proxy
function transformArticles(url, articles, category) {
  return articles.map( article => (
    {
      ...article,
      id: md5(article.title + article.publishedAt),
      urlToImage: `${url}/images?url=${article.urlToImage}`,
    })
  );
}

async function getHeadlines(url) {
  try {
    const response = await axios.get(`${apuUrl}?country=${country}&apiKey=${apiKey}`);
    return { timestamp: new Date(), articles: transformArticles(url, response.data.articles) };
  } catch (error) {
    if (error && error.response && error.response.data) {
      console.error(`getHeadlines() -> ${error.response.data.code} - ${error.response.data.message}`)
    } else {
      console.error(error.message);
    }
    throw error;
  };
}

async function getHeadlinesByCategory(url, category) {
  try {
    debugger;
    const response = await axios.get(`${apuUrl}?country=${country}&category=${category}&apiKey=${apiKey}`);
    return { timestamp: new Date(), articles: transformArticles(url, response.data.articles, category) };
  } catch (error) {
    if (error && error.response && error.response.data) {
      console.error(`getHeadlinesByCategory() -> ${error.response.data.code} - ${error.response.data.message}`)
    } else {
      console.error(error.message);
    }
    throw error;
  };
}

module.exports = {
  getHeadlines,
  getHeadlinesByCategory,
};