'use strict';
const express = require('express');
const router = express.Router();
const api = require('./api');

router.get('/headlines', (req, res, next) => {
  api.getHeadlines(req.baseUrl)
    .then(data => {
      res.json(data);
    })
    .catch(next);
});

router.get('/headlines/:category', (req, res, next) => {
  api.getHeadlinesByCategory(req.baseUrl, req.params.category)
    .then(data => {
      res.json(data);
    })
    .catch(next);
});

module.exports = router;
