'use strict';
const express = require('express');
const app = express();
const path = require("path");

//just for logging entry calls
app.use(function(req, res, next) {
    console.log("mock request -> " + req.url);
    next();
});

const filePath = path.join(__dirname, './testData.json');

app.get('/mock-data', (req, res, next) => {
  res.sendFile(filePath);
});

const port = 3010;
app.listen(port, () => console.log("Mock App listening on port " + port + "!"));
