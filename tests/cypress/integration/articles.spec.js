/// <reference types="Cypress" />

describe('Navigate to Articles', function() {

  it('Opens Article page', function() {
    cy.visit("/")

    cy.get('[data-test=headlineCard] [data-test=headlineTitle]').first().click();

    cy.location('pathname').should('contain', '/all/');

    cy.get('[data-test=articleTitle]').should('exist');
    cy.get('[data-test=articleBody]').should('exist');
    cy.get('[data-test=articleImage]').should('exist');

    cy.get('[data-test=backButton]').click();
    cy.wait(1000);
    cy.location('pathname').should('equal', '/all');
    
  })
})