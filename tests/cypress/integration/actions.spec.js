/// <reference types="Cypress" />

describe('Use actions', function() {

  it('Checks actions in headlines', function() {
    cy.visit("/business")

    //do not need to click here...
    cy.get('[data-test=openSourceButton]').first().should('have.attr', 'aria-label', 'Open');
    
    //again, do not click here...
    cy.get('[data-test=twitterButton]').first()
      .should('have.prop', 'href')
      .and('include', 'https://twitter.com/intent/tweet?url=');
  })
})