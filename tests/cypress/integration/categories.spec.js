/// <reference types='Cypress' />

describe('List of Categories', function() {

  it('Visit each category and check is selected', function() {
    cy.visit('/')
    cy.get('[data-test=categories]').contains('All').parentsUntil('have.class', '.Mui-selected');

    cy.visit('/business')
    cy.get('[data-test=categories]').contains('Business').parentsUntil('have.class', '.Mui-selected');

    cy.visit('/entertainment')
    cy.get('[data-test=categories]').contains('Entertainment').parentsUntil('have.class', '.Mui-selected');

    cy.visit('/general')
    cy.get('[data-test=categories]').contains('General').parentsUntil('have.class', '.Mui-selected');

    cy.visit('/health')
    cy.get('[data-test=categories]').contains('Health').parentsUntil('have.class', '.Mui-selected');

    cy.visit('/science')
    cy.get('[data-test=categories]').contains('Science').parentsUntil('have.class', '.Mui-selected');

    cy.visit('/sport')
    cy.get('[data-test=categories]').contains('Sport').parentsUntil('have.class', '.Mui-selected');

    cy.visit('/technology')
    cy.get('[data-test=categories]').contains('Technology').parentsUntil('have.class', '.Mui-selected');

    cy.visit('/all')
    cy.get('[data-test=categories]').contains('All').parentsUntil('have.class', '.Mui-selected');
  })
})