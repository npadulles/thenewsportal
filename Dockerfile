FROM node:10-alpine as build

WORKDIR /app

COPY client/package*.json ./
RUN npm ci --only=production

COPY client .

RUN npm run build
RUN rm -f ./build/precache-manifest.*.js ./build/service-worker.js



FROM node:10-alpine

WORKDIR /app

COPY server .
RUN npm install --prod

COPY --from=build /app/build ./build/

ENV API_KEY=${API_KEY} \
    NODE_ENV=production \
    PORT=80

EXPOSE 80

CMD [ "node", "index.js" ]