# "The News Portal" Demo PWA

This is a demo project to play with PWA capabilities

## Project Structure

* `client` contains create-react-app based application with [Material-UI](https://material-ui.com/) toolset
* `server` contains Express webserver providing static files (from `client`) and interacts with *newsapi*.
* `mock` this is a very simple webservice to serve a preloaded response. Used for local development to prevent hitting *newsapi* continuously
* `tests` functional tests based on [cypress](https://www.cypress.io/)

## Prerequisites

* Create a free account in [newsapi.org](https://newsapi.org/)
* *NodeJs 10.x* installed (for local development)
* *Docker* installed to have final image running

## Local Development Setup

You would need 2 terminals minimum to run *server* and *client* independently. Optionally a thrid terminal can be used to run *mock* server

### Server

*Server* requires some ENV variables to be configured:

* *API_KEY* - Set here your *newsapi* key
* *PORT* (optional) - Port used for webserver, default 3001
* *API_URL* (optional) - In case of using mock, set it to `"http://localhost:3010/mock-data"`. Default, use *newsapi* endpoint
* *API_COUNTRY* (optional) - The 2-letter ISO 3166-1 code of the country you want to get headlines for. Check [here](https://newsapi.org/docs/endpoints/top-headlines). Default "us"

These entries can be defined in a file `.env` inside server directory

```bash
cd server
npm install
API_KEY="<your_key_here>" npm start
```

### Client

Runs with default [CRA](https://facebook.github.io/create-react-app/) template without ejecting

```bash
cd client
npm install
npm start
```

### Mock (optional)

This will run mock server in port 3010. Predefined data loaded from file `testData.json`
When running *mock*, configure *server* to use this endpoint by *API_URL* env

```bash
cd mock
npm install
npm start
```

## Build & Run with docker

Run following commands to have a docker image running

```bash
#build
docker build . -t thenewsportal

#run
docker run -e API_KEY=<your_api_key> -p 8080:80 -d thenewsportal
```

Access your app using `http://localhost:8080`

> Take into account that PWA only works over HTTPS or 'localhost'

## Run Functional Tests with docker-compose

Functional tests by default are configured to run with *mock* data.

```bash
#build & run docker-compose
docker-compose up -d --build

#run tests
cd tests
npm install
CYPRESS_baseUrl=http://localhost:3001 npm test
```
