import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import OpenIcon from '@material-ui/icons/OpenInNewRounded';

function OpenButton(props) {

  const handleClick = (event) => {
    event.preventDefault();
    window.open(props.url, "_blank", "noopener,noreferrer");
  }

  return (
    <IconButton aria-label="Open" onClick={event => handleClick(event)} data-test="openSourceButton">
      <OpenIcon/>
    </IconButton>
  )
}

export default OpenButton;