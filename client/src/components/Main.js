import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Route, Switch } from 'react-router-dom';

import Categories from './Categories';
import Headlines from './Headlines';
import Article from './Article';

import RefreshDialog from './RefreshDialog';

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
  }, 
}));

function Main() {

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <RefreshDialog />
      <Route path="/:category?" component={Categories} />
      <Switch>
        <Route exact path="/:category/:id" component={Article} />
        <Route path="/:category?" component={Headlines} />
      </Switch>
    </div>
  );
}

export default Main;
