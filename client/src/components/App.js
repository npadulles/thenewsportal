import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Header from './Header';
import Main from './Main';

function App() {
  return (
    <React.Fragment>
      <CssBaseline />
      <Header />
      <Main />
    </React.Fragment>
  );
}

export default App;
