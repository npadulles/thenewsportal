import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MainIcon from '@material-ui/icons/LocalLibraryRounded';

const useStyles = makeStyles(theme => ({
  icon: {
    marginRight: theme.spacing(2),
  }, 
}));

function Header() {
  const classes = useStyles();

  return (
    <AppBar position="relative">
        <Toolbar>
          <MainIcon className={classes.icon} />
          <Typography variant="h6" color="inherit" noWrap>
            [ The News Portal ]
          </Typography>
        </Toolbar>
      </AppBar>
  );
}

export default Header;
