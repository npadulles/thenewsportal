import React from 'react';
// import { withRouter } from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AllIcon from '@material-ui/icons/DoneAllRounded';
import BusinessIcon from '@material-ui/icons/AccountBalanceRounded';
import EntertainmentIcon from '@material-ui/icons/LocalActivityRounded';
import GeneralIcon from '@material-ui/icons/TripOriginRounded';
import HealthIcon from '@material-ui/icons/LocalHospitalRounded';
import ScienceIcon from '@material-ui/icons/SchoolRounded';
import SportIcon from '@material-ui/icons/DirectionsRunRounded';
import TechnologyIcon from '@material-ui/icons/ComputerRounded';
import Hidden from '@material-ui/core/Hidden';

//for now this is fixed here due to simplicity
const categoryList = [
  { id: "all", name: "All", icon: <AllIcon />},
  { id: "business", name: "Business", icon: <BusinessIcon />},
  { id: "entertainment", name: "Entertainment", icon: <EntertainmentIcon />},
  { id: "general", name: "General", icon: <GeneralIcon />},
  { id: "health", name: "Health", icon: <HealthIcon />},
  { id: "science", name: "Science", icon: <ScienceIcon />},
  { id: "sport", name: "Sport", icon: <SportIcon />},
  { id: "technology", name: "Technology", icon: <TechnologyIcon />},
];


function Categories({history, match}) {

  const handleClick = (event, cat) => {
    event.preventDefault();
    history.push("/" + cat.id);
  };

  return (
    <List component="nav" data-test="categories">
      {categoryList.map( cat => 
        <ListItem button key={cat.id}
          onClick={event => handleClick(event, cat)}
          selected={cat.id === match.params.category || (cat.id === "all" && !match.params.category)}
        >
          <ListItemIcon>{ cat.icon }</ListItemIcon>
          <Hidden xsDown={true}>
            <ListItemText primary={cat.name} />
          </Hidden>
        </ListItem>
      )}
    </List>
  );
}

// export default withRouter(Categories);
export default Categories;
