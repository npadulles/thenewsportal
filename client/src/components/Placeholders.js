import React from 'react';
import ContentLoader from 'react-content-loader';

const MediaPlaceholder = () => (
  <ContentLoader 
    height={200}
    width={400}
    speed={2}
    primaryColor="#f3f3f3"
    secondaryColor="#ecebeb"
  >
    <rect x="0" y="0" rx="5" ry="5" width="400" height="200" />
  </ContentLoader>
);

const HeadlinePlaceholder = () => (
  <ContentLoader 
    height={160}
    width={400}
    speed={2}
    primaryColor="#f3f3f3"
    secondaryColor="#ecebeb"
  >
    <rect x="25" y="15" rx="5" ry="5" width="340" height="25" /> 
    <rect x="45" y="50" rx="5" ry="5" width="300" height="10" /> 
    <rect x="25" y="80" rx="5" ry="5" width="340" height="10" /> 
    <rect x="25" y="100" rx="5" ry="5" width="340" height="10" /> 
    <rect x="25" y="120" rx="5" ry="5" width="340" height="10" /> 
    <rect x="25" y="140" rx="5" ry="5" width="340" height="10" /> 
    <rect x="25" y="160" rx="5" ry="5" width="340" height="10" />
  </ContentLoader>
);

export { 
  MediaPlaceholder,
  HeadlinePlaceholder,
};
