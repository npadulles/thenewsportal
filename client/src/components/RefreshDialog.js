import React, { useState, useEffect, useRef } from 'react';
import { Workbox } from 'workbox-window';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

function RefreshDialog() {
  const [open, setOpen] = useState(false);
  const wb = useRef(null);

  function handleReload() {
    wb.current.addEventListener('controlling', (event) => {
      window.location.reload();
    });
    wb.current.messageSW({type: 'SKIP_WAITING'});
  }

  useEffect(() => {
    if ('serviceWorker' in navigator) {
      wb.current = new Workbox('/sw.js');
      
      // is SW installed but waiting to activate?
      wb.current.addEventListener('waiting', (event) => {
        if (event.isUpdate)
          setOpen(true);
      });
      wb.current.register();
    }
  }, []);

  return (
    <div>
      <Dialog
        open={open}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"There is a newer version of the site"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Please reload page to get updated content for the site
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleReload} color="primary" autoFocus>
            Reload
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default RefreshDialog;