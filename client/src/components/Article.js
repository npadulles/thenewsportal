import React, { useEffect, useState, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import BackIcon from '@material-ui/icons/ArrowBackRounded';
import Button from '@material-ui/core/Button';
import OpenButton from './OpenButton';
import TwitterButton from './TwitterButton';

import api from '../api';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  title: {
    paddingBottom: theme.spacing(4),
  },
  media: {
    width: "100%",
  },
  source: {
    textAlign: "right",
  },
  author: {
    textAlign: "right",
  },
  top: {
    paddingBottom: theme.spacing(2),
  },
}));

function Article({match, history}) {
  const id = match.params.id;
  const [article, setArticle] = useState({});

  const handleBack = useCallback(() => {
    const idx = match.url.lastIndexOf("/");
    const newUrl = match.url.substring(0, idx);
    history.push(newUrl);
    },
    [history, match.url]);

  useEffect(() => {
    const tempArticle = api.getArticle(id);
    if (!tempArticle) {
      //article is not in our local cache...
      //this can be enhanced to issue a new category load to server and check again if desired article is available
      handleBack();
    } else {
      setArticle(tempArticle);
    }
  }, [id, handleBack]);

  const classes = useStyles();

  return (
    <Container className={classes.root} maxWidth="lg">
      <Grid container spacing={0} className={classes.top}>
        <Grid item sm={6}>
          <Button size="small" onClick={() => handleBack()} data-test="backButton">
            <BackIcon/>Back
          </Button>
        </Grid>
        <Grid item sm={6} className={classes.source}>
          {article.source && article.source.name &&
          <Typography variant="caption">
            {article.source.name}
          </Typography>}
        </Grid>
      </Grid>
      <Typography variant="h4" className={classes.title} data-test="articleTitle">
        {article.title}
      </Typography>
      {article.description && <Typography variant="h6" className={classes.title}>
        {article.description}
      </Typography>}
      <Grid container spacing={4}>
        <Grid item sm={12} md={8}>
          <Typography variant="body1" data-test="articleBody">
            {article.content}
          </Typography>
          <Typography variant="body2" className={classes.author}>
            {article.author}
          </Typography>
          {article.url && <OpenButton url={article.url} />}
          {article.url && <TwitterButton url={article.url} />}
        </Grid>
        <Grid item sm={12} md={4}>
          <img src={article.urlToImage} alt="media" className={classes.media} data-test="articleImage"/>
        </Grid>
      </Grid>
    </Container>
);
}

export default Article;
