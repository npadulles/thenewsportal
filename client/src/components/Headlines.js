import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import OpenButton from './OpenButton';
import TwitterButton from './TwitterButton';
import Typography from '@material-ui/core/Typography';
import { MediaPlaceholder, HeadlinePlaceholder } from './Placeholders';

import api from '../api';

const useStyles = makeStyles(theme => ({
  headlinesGrid: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  headline: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  headlineMedia: {
    paddingTop: '56.25%', // 16:9
  },
  headlineContent: {
    flexGrow: 1,
  },
  title: {
    cursor: "pointer",
    fontWeight: "bold",
  },
  source: {
    marginLeft: "auto",
  }
}));

// initially show 6 placeholders
const placeholders = [1,2,3,4,5,6].map(i => ({
  id: i,
  placeholder: true,
}));

function Headlines({match, history}) {
  const category = match.params.category;
  const [headlines, setHeadlines] = useState(placeholders);

  //to get new data on category change
  useEffect(() => {
    setHeadlines(placeholders);
    const cat = category || "all";
    api.getHeadlines(cat).then(articles => setHeadlines(articles));
  }, [category]);

  const handleOpen = (event, id) => {
    event.preventDefault();
    const url = match.url && match.url !== "/" ? match.url : "/all";
    history.push(`${url}/${id}`);
  }

  const classes = useStyles();

  return (
    <Container className={classes.headlinesGrid} maxWidth="lg">
      <Grid container spacing={4}>
        {headlines === null && <Grid item>
            <Typography variant="h6">
              Oups! There was an error loading news. Check internet connection :(
            </Typography>
          </Grid>}
        {Array.isArray(headlines) && headlines.map(headline => (
          <Grid item key={headline.id} sm={12} md={6} lg={4}>
            <Card className={classes.headline} data-test="headlineCard">
              {headline.urlToImage && <CardMedia
                className={classes.headlineMedia}
                image={headline.urlToImage}
                component={headline.mediaPlaceholder}
                title="Image"
              />}
              {headline.placeholder && <CardMedia src="noop"
                className={classes.headlineMedia}
                component={MediaPlaceholder}
              />}
              <CardContent className={classes.headlineContent}>
                {headline.title && <Typography gutterBottom variant="h5" component="a" data-test="headlineTitle"
                  onClick={event => handleOpen(event, headline.id)} className={classes.title}>
                  {headline.title}
                </Typography>}
                {headline.description && <Typography>
                  {headline.description}
                </Typography>}
                {headline.placeholder && <HeadlinePlaceholder/>}
              </CardContent>
              <CardActions disableSpacing>
                {headline.url && <OpenButton url={headline.url} />}
                {headline.url && <TwitterButton url={headline.url} />}
                {headline.source && headline.source.name &&
                  <Typography variant="caption" className={classes.source}>
                  {headline.source.name}
                </Typography>}
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
);
}

export default Headlines;
