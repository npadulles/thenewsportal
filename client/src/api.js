import axios from "axios";

//extremely basic local cache :)
//wanted to keep only "latest" news, 
// if we want to keep historical articles, then using localStorage would be better
const loadedArticles = {};

const getHeadlines = async (category) => {
  const path = (category && category !== "all") ? `/api/headlines/${category}` : '/api/headlines';
  try {
    const response = await axios.get(path);
    const articles = response.data.articles;
    //do basic caching
    articles.forEach(a => {
      loadedArticles[a.id] = a;
    });
    return articles;
  } catch (error) {
    console.error(`Failed to get Headlines from network...`);
    return null;
  }
};

const getArticle = (id) => {
  //get from our basic cache 
  return loadedArticles[id];
};

export default {
  getHeadlines,
  getArticle,
}