/* eslint-disable */

//this file is only used for development, will be overwritten during prod build

if ('function' === typeof importScripts) {
  importScripts(
    'https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js'
  );

  // global workbox
  if (workbox) {
    console.log('Workbox is loaded');

    //used to handle page refresh
    addEventListener('message', (event) => {
      if (event.data && event.data.type === 'SKIP_WAITING') {
        skipWaiting();
      }
    });

    workbox.setConfig({
      debug: true
    });

    // custom cache rules
    workbox.routing.registerNavigationRoute('/index.html', {
      blacklist: [/^\/_/, /\/[^\/]+\.[^\/]+$/],
    });

    // cache images
    workbox.routing.registerRoute(
      /\/api\/images\?url=(.*)$/,
      new workbox.strategies.CacheFirst({
        cacheName: 'images',
        plugins: [
          new workbox.expiration.Plugin({
            maxEntries: 250,
            maxAgeSeconds: 2 * 24 * 60 * 60, // 2 Days
          }),
        ],
      })
    );

    // cache articles
    workbox.routing.registerRoute(
      /\/api\/headlines/,
      new workbox.strategies.StaleWhileRevalidate({
        cacheName: 'articles',
      })
    );

  } else {
    console.log('Workbox could not be loaded. No Offline support');
  }
}